swagger: "2.0"
info:
  description: "02267 - DTU Pay - Group 2"
  version: "1.0.0"
  title: "DTU Pay"
host: "g-02.compute.dtu.dk:8080"
tags:
  - name: "customers"
    description: "Endpoint dealing with customer registration"
  - name: "merchants"
    description: "Endpoint dealing with merchant registration"
  - name: "payments"
    description: "Endpoint dealing with money transfers"
  - name: "reports"
    description: "Endpoint allowing the manager to extract reporting info from the system"
  - name: "tokens"
    description: "Endpoint allowing a customer to manage his access tokens for DTU Pay"
  - name: "healthcheck"
    description: "Endpoint to verify that the system is running and available for communication"
schemes:
  - "http"
paths:
  /customers:
    post:
      tags:
        - "customers"
      summary: "Register as a new customer"
      consumes:
        - "application/json"
      produces:
        - "application/json"
      parameters:
        - in: "body"
          name: "body"
          description: "Customer object that needs to be registered with DTU Pay. The ID should be the ID of the bank account."
          required: true
          schema:
            $ref: "#/definitions/Customer"
      responses:
        "201":
          description: "Customer created"
        "400":
          description: "Bad request - contains error description"
  /merchants:
    post:
      tags:
        - "merchants"
      summary: "Register as a new merchant"
      consumes:
        - "application/json"
      produces:
        - "application/json"
      parameters:
        - in: "body"
          name: "body"
          description: "Merchant object that needs to be registered with DTU Pay. The ID should be the ID of the bank account."
          required: true
          schema:
            $ref: "#/definitions/Merchant"
      responses:
        "201":
          description: "Merchant created"
        "400":
          description: "Bad request - contains error description"
  /payments:
    post:
      tags:
        - "payments"
      summary: "Merchant can initiate payment"
      consumes:
        - "application/json"
      produces:
        - "application/json"
      parameters:
        - in: "body"
          name: "body"
          description: "Payment object containing all required information for a payment"
          required: true
          schema:
            $ref: "#/definitions/Payment"
      responses:
        "201":
          description: "Payment created (money transfer successful)"
        "400":
          description: "Bad request - contains error description"
  /reports:
    get:
      tags:
        - "reports"
      summary: "Endpoint for manager to retrieve a list of all transactions"
      consumes:
        - "application/json"
      produces:
        - "application/json"
      responses:
        "200":
          description: "OK - contains a list of transactions"
          schema:
            type: "array"
            items:
              $ref: "#/definitions/Transaction"
  /reports/{cid}:
    get:
      tags:
        - "reports"
      summary: "Endpoint for manager to retrieve a list of all transactions for a customer"
      consumes:
        - "application/json"
      produces:
        - "application/json"
      parameters:
        - name: "cid"
          in: "path"
          description: "Customer ID for whom the manager wants a list of transactions"
          required: true
          type: "string"
      responses:
        "200":
          description: "OK - contains a list of transactions for the given customer"
          schema:
            type: "array"
            items:
              $ref: "#/definitions/Transaction"
  /tokens:
    post:
      tags:
        - "tokens"
      summary: "Customer endpoint to request new tokens"
      consumes:
        - "application/json"
      produces:
        - "application/json"
      parameters:
        - in: "body"
          name: "body"
          description: "CreateToken object specifying the id of the customer and the desired amount of tokens"
          required: true
          schema:
            $ref: "#/definitions/CreateToken"
      responses:
        "201":
          description: "Created - Contains a list of the created tokens"
          schema:
            type: "array"
            items:
              $ref: "#/definitions/Token"
        "400":
          description: "Bad request - contains error description"
  /tokens/clear:
    post:
      tags:
        - "tokens"
      summary: "Customer endpoint to clear his tokens"
      consumes:
        - "application/json"
      produces:
        - "application/json"
      parameters:
        - in: "body"
          name: "body"
          description: "ClearToken object specifying the id of thecustomer"
          required: true
          schema:
            $ref: "#/definitions/ClearToken"
      responses:
        "204":
          description: "No content - successfully cleared the customers tokens"
        "400":
          description: "Bad request - contains error description"
  /healthcheck:
    get:
      tags:
        - "healthcheck"
      summary: "Endpoint with no dependencies. Used to check the health of the service"
      responses:
        "200":
          description: "OK - The service is healthy and open for communication"

definitions:
  Customer:
    type: "object"
    properties:
      id:
        type: "string"
  Merchant:
    type: "object"
    properties:
      id:
        type: "string"
  Payment:
    type: "object"
    properties:
      mid:
        type: string
      amount:
        type: number
      description:
        type: string
      token:
        $ref: "#/definitions/Token"
  Token:
    type: "object"
    properties:
      uuid:
        type: string
      isValid:
        type: boolean
  CreateToken:
    type: "object"
    properties:
      cid:
        type: string
      amount:
        type: number
  ClearToken:
    type: "object"
    properties:
      cid:
        type: string
  Transaction:
    type: "object"
    properties:
      cid:
        type: string
      mid:
        type: string
      amount:
        type: number
      description:
        type: string
      date:
        type: string
      token:
        $ref: "#/definitions/Token"

package messaging;

import java.util.concurrent.CompletableFuture;

/**
 * Allow for easy access to the computable futures of events.
 */
public class FutureEvent {
    private final Event expectedEvent;
    private final CompletableFuture<Event> eventFuture;

    public FutureEvent(Event expectedEvent, CompletableFuture<Event> eventFuture) {
        this.expectedEvent = expectedEvent;
        this.eventFuture = eventFuture;
    }

    public Event getExpectedEvent() {
        return expectedEvent;
    }

    public CompletableFuture<Event> getEventFuture() {
        return eventFuture;
    }
}

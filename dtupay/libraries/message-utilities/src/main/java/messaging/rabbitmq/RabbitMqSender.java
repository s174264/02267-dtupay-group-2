package messaging.rabbitmq;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import messaging.Event;
import messaging.EventSender;

import java.nio.charset.StandardCharsets;

/**
 * Borrowed from the course example projects.
 * Edited to support this projects use cases.
 */
public class RabbitMqSender implements EventSender {
    private static final String EXCHANGE_NAME = "eventsExchange";
    private static final String QUEUE_TYPE = "topic";
    private static final String TOPIC = "events";

    private final String rabbitHost;
    private final String rabbitPrefix;
    private int rabbitPort = 5672;

    public RabbitMqSender(String host, int port, String rabbitPrefix) {
        this.rabbitHost = host;
        this.rabbitPort = port;
        this.rabbitPrefix = rabbitPrefix;
    }

    public RabbitMqSender(String host, String rabbitPrefix) {
        this.rabbitHost = host;
        this.rabbitPrefix = rabbitPrefix;
    }

    @Override
    public void sendEvent(Event event) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(rabbitHost);
        factory.setPort(rabbitPort);
        try (Connection connection = factory.newConnection(); Channel channel = connection.createChannel()) {
            channel.exchangeDeclare(EXCHANGE_NAME, QUEUE_TYPE);
            String message = new Gson().toJson(event);
            System.out.println(rabbitPrefix + ": [x] sending " + message);
            channel.basicPublish(EXCHANGE_NAME, TOPIC, null, message.getBytes(StandardCharsets.UTF_8));
        }
    }
}

package messaging.rabbitmq;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import messaging.Event;
import messaging.EventReceiver;

import java.nio.charset.StandardCharsets;

/**
 * Borrowed from the course example projects.
 * Edited to support this projects use cases.
 */
public class RabbitMqListener {

    private static final String EXCHANGE_NAME = "eventsExchange";
    private static final String QUEUE_TYPE = "topic";
    private static final String TOPIC = "events";

    private final EventReceiver service;
    private final String rabbitHost;
    private final String rabbitPrefix;
    private int rabbitPort = 5672;

    public RabbitMqListener(EventReceiver service, String host, int port, String prefix) {
        this.service = service;
        this.rabbitHost = host;
        this.rabbitPort = port;
        this.rabbitPrefix = prefix;
    }

    public RabbitMqListener(EventReceiver service, String host, String prefix) {
        this.service = service;
        this.rabbitHost = host;
        this.rabbitPrefix = prefix;
    }

    public void listen() throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(rabbitHost);
        factory.setPort(rabbitPort);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare(EXCHANGE_NAME, QUEUE_TYPE);
        String queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, EXCHANGE_NAME, TOPIC);

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            System.out.println(rabbitPrefix + ": [x] receiving " + message);

            Event event = new Gson().fromJson(message, Event.class);
            try {
                service.receiveEvent(event);
            } catch (Exception e) {
                throw new Error(e);
            }
        };

        channel.basicConsume(queueName, true, deliverCallback, consumerTag -> {
        });
    }
}

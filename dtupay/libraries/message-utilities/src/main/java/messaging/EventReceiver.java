package messaging;

/**
 * Borrowed from the course example projects.
 */
public interface EventReceiver {
	void receiveEvent(Event event) throws Exception;
}

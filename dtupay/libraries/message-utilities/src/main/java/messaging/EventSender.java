package messaging;

/**
 * Borrowed from the course example projects.
 */
public interface EventSender {
	void sendEvent(Event event) throws Exception;
}

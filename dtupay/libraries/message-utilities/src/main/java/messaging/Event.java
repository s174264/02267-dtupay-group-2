package messaging;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.util.*;
import java.util.stream.Collectors;


/**
 * Borrowed from the course example projects.
 * Edited to support this projects use cases.
 */
public class Event {
    private final UUID correlationId;
    private final UUID eventId = UUID.randomUUID();

    private final String eventType;
    private final String eventAction;
    private Object[] arguments = null;

    private boolean hasError;
    private String errorMessage;

    public Event(UUID correlationId, String eventType, String eventAction) {
        this.correlationId = correlationId;
        this.eventType = eventType;
        this.eventAction = eventAction;
    }

    public Event(UUID correlationId, String eventType, String eventAction, Object[] arguments) {
        this.correlationId = correlationId;
        this.eventType = eventType;
        this.eventAction = eventAction;
        this.arguments = arguments;
    }

    public Event(UUID correlationId, String eventType, String eventAction, boolean hasError, String errorMessage) {
        this.correlationId = correlationId;
        this.eventType = eventType;
        this.eventAction = eventAction;
        this.hasError = hasError;
        this.errorMessage = errorMessage;
    }

    public UUID getEventId() {
        return eventId;
    }

    public UUID getCorrelationId() {
        return correlationId;
    }

    public String getEventType() {
        return eventType;
    }

    public String getEventAction() {
        return eventAction;
    }

    public Object[] getArguments() {
        return arguments;
    }

    public boolean getHasError() {
        return hasError;
    }

    public void setHasError(boolean hasError) {
        this.hasError = hasError;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean same(Event event) {
        return correlationId.equals(event.getCorrelationId()) &&
                eventType.equals(event.getEventType()) &&
                eventAction.equals(event.getEventAction());
    }

    public boolean equals(Event event) {
        return eventId.equals(event.getEventId());
    }

    /**
     * @author Oscar
     * This method allows to extract the argument of an event and parse it into the expected class.
     */
    public <T> T getArgumentsAsObject(Class<T> classType) {
        JsonObject jsonObject = new Gson().toJsonTree(arguments[0]).getAsJsonObject();
        return new Gson().fromJson(jsonObject, classType);
    }

    /**
     * @author Oscar
     * This method allows to extract the argument of an event and parse it into an array.
     */
    public <T> List<T> getArgumentsAsArray(Class<T> classType) {
        JsonArray jsonArray = new Gson().toJsonTree(arguments).getAsJsonArray();
        return new Gson().fromJson(jsonArray, new TypeToken<List<T>>() {
        }.getType());
    }

    /**
     * @author Frederik
     * This method allows to extract the argument of an event and parse it into a list of an expected class.
     */
    public <T> List<T> getArgumentsAsObjectArray(Class<T> classType) {
        List<T> list = new ArrayList<>();
        for (Object argument : arguments) {
            // Create dummy event. This will allow us to call getArgumentAsObject() to get the argument that we expect.
            list.add(new Event(UUID.randomUUID(), "b", "c", new Object[]{argument}).getArgumentsAsObject(classType));
        }

        return list;
    }

    public String toString() {
        List<String> strs = new ArrayList<>();
        if (arguments != null) {
            List<Object> objs = Arrays.asList(arguments);
            strs = objs.stream().map(o -> o.toString()).collect(Collectors.toList());
        }

        return String.format("event(%s,%s,%s)", eventType, eventAction, String.join(",", strs));
    }
}

package test;

import dtubank.BankService;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.EventSender;
import payment.PaymentService;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PaymentServiceSteps {
    PaymentService service;
    Event event;
    BankService bank = new BankServiceMocker();
    UUID correlationId = UUID.randomUUID();
    PaymentHelper paymentHelper = new PaymentHelper();

    public PaymentServiceSteps() {
        service = new PaymentService(bank, new EventSender() {
            @Override
            public void sendEvent(Event ev) throws Exception {
                event = ev;
            }
        });
    }

    /** @author Helena */
    @Given("there is a successful payment")
    public void thereIsASuccessfulPayment() {
        paymentHelper.getSomePayment().description = "Should succeed";
    }

    /** @author Zenia */
    @Given("there is a failing payment")
    public void thereIsAFailingPayment() {
        paymentHelper.getSomePayment().description = "Should fail";
    }

    /** @author Helena */
    @When("I receive event with type {string} and action {string}")
    public void iReceiveEventWithTypeAndAction(String eventType, String eventAction) throws Exception {
        Object[] object = new Object[]{paymentHelper.getPayment()};
        service.receiveEvent(new Event(correlationId, eventType, eventAction, object) );
    }

    /** @author Mai-Thi */
    @Then("I have sent event type {string} and action {string}")
    public void iHaveSentEventTypeAndAction(String eventType, String eventAction) {
        assertEquals(correlationId, event.getCorrelationId());
        assertEquals(eventType, event.getEventType());
        assertEquals(eventAction, event.getEventAction());
    }

    /** @author Mai-Thi */
    @Then("the event has the object {string}")
    public void theEventHasTheObject(String arg0) {
        assertEquals(arg0, event.getArguments()[0]);
    }

    /** @author Zenia */
    @Then("the event has an errormessage")
    public void theEventHasAnErrormessage() {
       assertTrue(event.getHasError());
    }
}

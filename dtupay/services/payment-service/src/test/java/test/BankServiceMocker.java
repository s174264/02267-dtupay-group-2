package test;

import dtubank.*;

import java.math.BigDecimal;
import java.util.List;

/** @author Emil */
public class BankServiceMocker implements BankService {
    @Override
    public Account getAccount(String accountId) throws BankServiceException_Exception {
        return null;
    }

    @Override
    public Account getAccountByCprNumber(String cpr) throws BankServiceException_Exception {
        return null;
    }

    @Override
    public String createAccountWithBalance(User user, BigDecimal balance) throws BankServiceException_Exception {
        return null;
    }

    @Override
    public void retireAccount(String accountId) throws BankServiceException_Exception {

    }

    @Override
    public List<AccountInfo> getAccounts() {
        return null;
    }

    @Override
    public void transferMoneyFromTo(String debtor, String creditor, BigDecimal amount, String description) throws BankServiceException_Exception {
        if (description.equals("Should fail")) {
            throw new BankServiceException_Exception("mock exception",new BankServiceException());
        }
    }
}

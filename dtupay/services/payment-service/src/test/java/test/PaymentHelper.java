package test;

import dto.Payment;

import java.math.BigDecimal;
import java.util.UUID;

/**
 * @author Mai-Thi & Zenia
 */
public class PaymentHelper {
    Payment payment;
    String token = UUID.randomUUID().toString();

    public PaymentHelper() {
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public Payment getSomePayment () {
        if (payment == null) {
             payment = new Payment("cid1", "mid1", new BigDecimal(200), "Helper", token);

        }

        return payment;
    }
}

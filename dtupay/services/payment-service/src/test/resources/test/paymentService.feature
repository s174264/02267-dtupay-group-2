Feature: PaymentService feature
  # @author Mai-Thi
  Scenario: Successful payment
    Given there is a successful payment
    When I receive event with type "payment" and action "create"
    Then I have sent event type "dtuPay" and action "createdPayment"
    And the event has the object "OK"

    # @author Zenia
  Scenario: Failure payment
    Given there is a failing payment
    When I receive event with type "payment" and action "create"
    Then I have sent event type "dtuPay" and action "createdPayment"
    And the event has an errormessage
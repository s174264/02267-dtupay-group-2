package dto;

import java.math.BigDecimal;

public class Payment {
    public String cid;
    public String mid;
    public BigDecimal amount;
    public String description;
    public String token;

    public Payment() {
    }

    public Payment(String cid, String mid, BigDecimal amount, String description, String token) {
        this.cid = cid;
        this.mid = mid;
        this.amount = amount;
        this.description = description;
        this.token = token;
    }
}
package payment;

import dto.Payment;
import dto.Transaction;
import dtubank.BankService;
import dtubank.BankServiceException_Exception;
import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;
import java.util.UUID;

/** @author Helena */
public class PaymentService implements EventReceiver {
    private final EventSender eventSender;
    private final BankService bank;

    public PaymentService(BankService bank, EventSender eventSender) {
        this.bank = bank;
        this.eventSender = eventSender;
    }

    @Override
    public void receiveEvent(Event event) throws Exception {
        if (event.getEventType().equals("payment")) {
            System.out.println("payment-service: handling event: " + event);

            // Creates a payments
            if (event.getEventAction().equals("create")) {
                Payment payment = event.getArgumentsAsObject(Payment.class);
                Event responseEvent;
                try {
                    bank.transferMoneyFromTo(payment.cid, payment.mid, payment.amount, payment.description);
                    responseEvent = new Event(event.getCorrelationId(), "dtuPay", "createdPayment", new Object[]{"OK"});

                    Transaction transaction = new Transaction(payment.cid, payment.mid, payment.amount, payment.description, payment.token);
                    Event reportEvent = new Event(UUID.randomUUID(), "report", "saveTransaction", new Object[]{transaction});
                    eventSender.sendEvent(reportEvent);

                } catch (BankServiceException_Exception e) {
                    // If the bank throws an exception, something went wrong
                    // And the payment was not successful
                    responseEvent = new Event(event.getCorrelationId(), "dtuPay", "createdPayment", true, e.getMessage());
                }
                eventSender.sendEvent(responseEvent);

            }
        } else {
            System.out.println("payment-service: event ignored: " + event);
        }
    }
}

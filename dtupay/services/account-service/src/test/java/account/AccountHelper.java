package account;

import messaging.Event;

/** @author Frederik */
public class AccountHelper {
    Event event;

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}

package account;

import messaging.Event;
import messaging.EventSender;

/** @author Frederik */
//Mock AccountService so the same instance can be used in the tests, to ensure both the customer and merchant are created and persisted.
//This extra file is only needed as we have to override the sendEvent method
public class MockAccountService extends AccountService {
    AccountHelper accountHelper;

    public MockAccountService(AccountHelper accountHelper) {
        super(new EventSender() {

            @Override
            public void sendEvent(Event ev) throws Exception {
                accountHelper.setEvent(ev);
            }
        });

        this.accountHelper = accountHelper;
    }
}

package account;

import dto.Merchant;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.EventSender;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MerchantSteps {
    AccountService accountService;
    AccountHelper accountHelper;

    public MerchantSteps(MockAccountService mockAccountService, AccountHelper accountHelper) {
        this.accountHelper = accountHelper;
        accountService = mockAccountService;
    }

    /** @author Frederik */
    @Given("A merchant with mid {string}")
    public void aMerchantWithMid(String mid) {
        List<Merchant> merchants = new ArrayList<>();
        merchants.add(new Merchant(mid));

        accountService.setMerchants(merchants);
    }

    /** @author Emil */
    @When("I receive the event with type {string} and action {string} and mid {string}")
    public void iReceiveTheEventWithTypeAndActionAndCid(String type, String action, String mid) throws Exception {
        Merchant merchant = new Merchant(mid);
        accountService.receiveEvent(new Event(UUID.randomUUID(), type, action, new Object[]{merchant}));
    }
}


package account;

import static org.junit.jupiter.api.Assertions.*;

import dto.Customer;
import dto.Payment;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CustomerSteps {
    AccountService accountService;
    AccountHelper accountHelper;

    public CustomerSteps(MockAccountService mockAccountService, AccountHelper accountHelper) {
        this.accountHelper = accountHelper;
        accountService = mockAccountService;
    }

    /** @author Frederik */
    @Given("A customer with cid {string}")
    public void aCustomerWithCid(String cid) {
        List<Customer> customers = new ArrayList<>();
        customers.add(new Customer(cid));

        accountService.setCustomers(customers);
    }

    /** @author Emil */
    @When("I receive the event with type {string} and action {string} and cid {string}")
    public void iReceiveTheEventWithTypeAndActionAndCid(String type, String action, String cid) throws Exception {
        Customer customer = new Customer(cid);
        accountService.receiveEvent(new Event(UUID.randomUUID(), type, action, new Object[]{customer}));
    }

    /** @author Oscar */
    @When("I receive the event with type {string} and action {string} and cid {string} and mid {string}")
    public void iReceiveTheEventWithTypeAndActionAndCidAndMid(String type, String action, String cid, String mid) throws Exception {
        Payment payment = new Payment(cid, mid, 10, "Description", UUID.randomUUID().toString());
        accountService.receiveEvent(new Event(UUID.randomUUID(), type, action, new Object[]{payment}));
    }

    /** @author Frederik */
    @Then("I send the event with type {string} and action {string}")
    public void iSendTheEventWithTypeAndAction(String type, String action) {
        assertEquals(type, accountHelper.getEvent().getEventType());
        assertEquals(action, accountHelper.getEvent().getEventAction());
    }

    /** @author Oscar */
    @Then("I send the event with type {string} and action {string} and error message {string}")
    public void iSendTheEventWithTypeAndActionAndErrorMessage(String type, String action, String errorMessage) {
        assertEquals(type, accountHelper.getEvent().getEventType());
        assertEquals(action, accountHelper.getEvent().getEventAction());
        assertEquals(errorMessage, accountHelper.getEvent().getErrorMessage());
    }
}


Feature: Register customer
  # @author Frederik
  Scenario: Customer successfully registers
    When I receive the event with type "account" and action "createCustomer" and cid "cid"
    Then I send the event with type "dtuPay" and action "createdCustomer"

    # @author Frederik
  Scenario: Customer already exists
    Given A customer with cid "cid"
    When I receive the event with type "account" and action "createCustomer" and cid "cid"
    Then I send the event with type "dtuPay" and action "createdCustomer" and error message "The customer is already registered with DTU Pay"
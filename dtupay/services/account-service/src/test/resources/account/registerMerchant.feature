Feature: Register merchant
  # @author Emil
  Scenario: Merchant successfully registers
    When I receive the event with type "account" and action "createMerchant" and mid "mid"
    Then I send the event with type "dtuPay" and action "createdMerchant"

    # @author Emil
  Scenario: Merchant already exists
    Given A merchant with mid "mid"
    When I receive the event with type "account" and action "createMerchant" and mid "mid"
    Then I send the event with type "dtuPay" and action "createdMerchant" and error message "The merchant is already registered with DTU Pay"
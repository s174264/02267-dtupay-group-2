Feature: Validate payment
  # @author Oscar
  Scenario: Payment successfully validated
    Given A customer with cid "cid"
    And A merchant with mid "mid"
    When I receive the event with type "account" and action "validate" and cid "cid" and mid "mid"
    Then I send the event with type "payment" and action "create"

  # @author Oscar
  Scenario: Customer doesn't exist
    Given A merchant with mid "mid"
    When I receive the event with type "account" and action "validate" and cid "cid" and mid "mid"
    Then I send the event with type "dtuPay" and action "createdPayment" and error message "Customer is not registered with DTUPay"

    # @author Oscar
  Scenario: Merchant doesn't exist
    Given A customer with cid "cid"
    When I receive the event with type "account" and action "validate" and cid "cid" and mid "mid"
    Then I send the event with type "dtuPay" and action "createdPayment" and error message "Merchant is not registered with DTUPay"
package account;

import dto.Customer;
import dto.Merchant;
import dto.Payment;
import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;

import java.util.ArrayList;
import java.util.List;

public class AccountService implements EventReceiver {
    private final EventSender eventSender;

    private List<Customer> customers = new ArrayList<>();
    private List<Merchant> merchants = new ArrayList<>();

    public AccountService(EventSender eventSender) {
        this.eventSender = eventSender;
    }

    /** @author Frederik */
    private void createCustomer(String cid) throws Exception {
        // Don't add duplicated customers
        if (customers.stream().anyMatch(c -> c.id.equals(cid))) {
            throw new Exception("The customer is already registered with DTU Pay");
        }

        Customer customer = new Customer(cid);
        this.customers.add(customer);
    }

    /** @author Frederik */
    private void createMerchant(String mid) throws Exception {
        if (merchants.stream().anyMatch(m -> m.id.equals(mid))) {
            throw new Exception("The merchant is already registered with DTU Pay");
        }

        Merchant merchant = new Merchant(mid);
        this.merchants.add(merchant);
    }

    /** @author Frederik */
    @Override
    public void receiveEvent(Event event) throws Exception {
        if (!event.getEventType().equals("account")) {
            System.out.println("account-service: event ignored: " + event);

            return;
        }

        switch (event.getEventAction()) {
            case "createCustomer": {
                Customer customer = event.getArgumentsAsObject(Customer.class);

                try {
                    createCustomer(customer.id);
                } catch (Exception err) {
                    Event e = new Event(event.getCorrelationId(), "dtuPay", "createdCustomer", true, err.getMessage());
                    eventSender.sendEvent(e);

                    break;
                }

                Event e = new Event(event.getCorrelationId(), "dtuPay", "createdCustomer");
                eventSender.sendEvent(e);

                break;
            }

            case "createMerchant": {
                Merchant merchant = event.getArgumentsAsObject(Merchant.class);
                try {
                    createMerchant(merchant.id);
                } catch (Exception err) {
                    Event e = new Event(event.getCorrelationId(), "dtuPay", "createdMerchant", true, err.getMessage());
                    eventSender.sendEvent(e);

                    break;
                }

                Event e = new Event(event.getCorrelationId(), "dtuPay", "createdMerchant");
                eventSender.sendEvent(e);

                break;
            }

            case "validate": {
                Payment payment = event.getArgumentsAsObject(Payment.class);

                if (customers.stream().noneMatch(c -> c.id.equals(payment.cid))) {
                    // DTUPay expects a message with type dtuPay and action createdPayment.
                    // This message will be received on dtuPay and so the error will be handled accordingly.
                    Event e = new Event(event.getCorrelationId(), "dtuPay", "createdPayment", true, "Customer is not registered with DTUPay");
                    eventSender.sendEvent(e);

                    break;
                }

                if (merchants.stream().noneMatch(m -> m.id.equals(payment.mid))) {
                    // DTUPay expects a message with type dtuPay and action createdPayment.
                    // This message will be received on dtuPay and so the error will be handled accordingly.
                    Event e = new Event(event.getCorrelationId(), "dtuPay", "createdPayment", true, "Merchant is not registered with DTUPay");
                    eventSender.sendEvent(e);

                    break;
                }

                Event e = new Event(event.getCorrelationId(), "payment", "create", new Object[] {payment});
                eventSender.sendEvent(e);
                break;
            }
        }
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    public void setMerchants(List<Merchant> merchants) {
        this.merchants = merchants;
    }
}

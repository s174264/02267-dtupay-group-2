import account.AccountService;
import messaging.EventSender;
import messaging.rabbitmq.RabbitMqListener;
import messaging.rabbitmq.RabbitMqSender;

public class StartUp {
    public static void main(String[] args) throws Exception {
        // Local or Docker connection to Rabbit Mq
        // Local: 127.0.0.1
        // Docker: rabbitmq
        String rabbitHost = "127.0.0.1";
        // If a system variable is set specifying message queue host use it. Otherwise 127.0.0.1 is used (localhost).
        if (System.getenv().containsKey("RABBITMQ_HOST") && !System.getenv("RABBITMQ_HOST").equals("")) {
            rabbitHost = System.getenv("RABBITMQ_HOST");
        }

        new StartUp().startUp(rabbitHost);
    }

    private void startUp(String host) throws Exception {
        EventSender s = new RabbitMqSender(host, "account-service");
        AccountService service = new AccountService(s);
        new RabbitMqListener(service, host, "account-service").listen();
    }
}

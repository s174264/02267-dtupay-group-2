package dto;

public class ReportParameter {
    public String cid;
    public String startDate;
    public String endDate;

    public ReportParameter() {
    }

    public ReportParameter(String cid, String startDate, String endDate) {
        this.cid = cid;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getCid() {
        return cid;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

}

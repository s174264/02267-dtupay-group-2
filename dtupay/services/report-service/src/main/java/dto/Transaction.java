package dto;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Transaction {
    public String cid;
    public String mid;
    public BigDecimal amount;
    public String description;
    public String date;
    public String token;

    @Override
    public boolean equals(Object o) {
        return ((Transaction)o).cid.equals(cid) &&
                ((Transaction)o).mid.equals(mid) &&
                ((Transaction)o).amount.equals(amount) &&
                ((Transaction)o).description.equals(description) &&
                ((Transaction)o).date.equals(date) &&
                ((Transaction)o).token.equals(token);
    }

    public Transaction(String cid, String mid, BigDecimal amount, String description, String token) {
        this.cid = cid;
        this.mid = mid;
        this.amount = amount;
        this.description = description;
        this.token = token;
        this.date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());
    }

    public Transaction(String cid, String mid, BigDecimal amount, String description, String token, String date) {
        this.cid = cid;
        this.mid = mid;
        this.amount = amount;
        this.description = description;
        this.token = token;
        this.date = date;
    }
}

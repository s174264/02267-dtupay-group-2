package report;

import dto.ReportParameter;
import dto.Transaction;
import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;

import java.text.SimpleDateFormat;
import java.util.*;

/** @author Mai-Thi */
public class ReportService implements EventReceiver {
    private final EventSender eventSender;
    private final List<Transaction> transactions = new ArrayList<>();

    public ReportService(EventSender eventSender) {
        this.eventSender = eventSender;
    }

    @Override
    public void receiveEvent(Event event) throws Exception {
        if (event.getEventType().equals("report")) {
            System.out.println("report-service: handling event: "+event);

            // Saves the transaction from the event
            if (event.getEventAction().equals("saveTransaction")) {
                transactions.add(event.getArgumentsAsObject(Transaction.class));

            // Sends an event with a list of all the transactions in DTUPay
            } else if (event.getEventAction().equals("allTransactions")){
                Event allTransactions = new Event(event.getCorrelationId(), "dtuPay", event.getEventAction(), transactions.toArray());
                eventSender.sendEvent(allTransactions);

            // Sends an event with a list of the customers transactions in a date interval
            } else if (event.getEventAction().equals("customerTransactions")){
                ReportParameter arguments = event.getArgumentsAsObject(ReportParameter.class);
                String cid = arguments.getCid();
                Date startDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(arguments.getStartDate() + " 00:00:00.000");
                Date endDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(arguments.getEndDate() + " 23:59:59.999");

                List<Transaction> customerTransactions = new ArrayList<>();
                // Filters all transactions that fulfills the specifications
                for (Transaction transaction : transactions){
                    if (transaction.cid.equals(cid)){
                        Date transDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(transaction.date);
                        if (!transDate.before(startDate) && !transDate.after(endDate)) {
                            customerTransactions.add(transaction);
                        }
                    }
                }

                Event customerTransactionsEvent = new Event(event.getCorrelationId(), "dtuPay", event.getEventAction(), customerTransactions.toArray());
                eventSender.sendEvent(customerTransactionsEvent);
            }
        } else {
            System.out.println("report-service: event ignored: "+event);
        }
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void addTransaction(Transaction transaction) {
        transactions.add(transaction);
    }
}

package report;

import messaging.Event;
import messaging.EventSender;

/** @author Zenia */
public class MockReportService extends ReportService {
    ReportHelper reportHelper;

    public MockReportService(ReportHelper reportHelper) {
        super(new EventSender() {

            @Override
            public void sendEvent(Event ev) throws Exception {
                reportHelper.setEvent(ev);
            }
        });

        this.reportHelper = reportHelper;
    }
}

package report;

import dto.ReportParameter;
import dto.Transaction;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReportSteps {
    ReportService reportService;
    ReportHelper reportHelper;

    String startDate = "";
    String endDate = "";

    public ReportSteps(MockReportService mockReportService, ReportHelper reportHelper) {
        reportService = mockReportService;
        this.reportHelper = reportHelper;
    }

    /** @author Emil */
    @Given("A transaction with cid {string} and date {string}")
    public void aTransactionWithTheDate(String cid, String date) {
        reportService.addTransaction(new Transaction(cid, "mid", new BigDecimal(10), "desc", "token", date + " 00:00:00.000"));
    }

    /** @author Oscar */
    @Given("A date interval with startDate {string} and endDate {string}")
    public void aDateIntervalWithStartDateAndEndDate(String startDate, String endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    /** @author Frederik */
    @When("I receive an event with type {string} and action {string} and cid {string} and date interval")
    public void iReceiveAnEventWithTypeAndActionAndStartDateAndEndDate(String type, String action, String cid) throws Exception {
        ReportParameter reportParameter = new ReportParameter(cid, startDate, endDate);
        reportService.receiveEvent(new Event(UUID.randomUUID(), type, action, new Object[]{reportParameter}));
    }

    /** @author Oscar */
    @Then("I send an event with type {string} and action {string}")
    public void iSendAndEventWithTypeAndAction(String eventType, String eventAction) {
        assertEquals(eventType, reportHelper.getEvent().getEventType());
        assertEquals(eventAction, reportHelper.getEvent().getEventAction());
    }

    /** @author Frederik */
    @Then("The sent event contains a list of all transactions")
    public void theSentEventContainsAListOfAllTransactions() {
        List<Transaction> sentTransactions = reportHelper.getEvent().getArgumentsAsObjectArray(Transaction.class);
        assertTrue(sentTransactions.containsAll(reportService.getTransactions()));
        assertTrue(reportService.getTransactions().containsAll(sentTransactions));
    }

    /** @author Emil */
    @Then("The sent event contains a list of transactions in the interval")
    public void theSentEventContainsAListOfTransactionsInTheIntervalTo() throws ParseException {
        Date startDateFormatted = new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
        Date endDateFormatted = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);

        List<Transaction> sentTransactions = reportHelper.getEvent().getArgumentsAsObjectArray(Transaction.class);
        for (var transaction : sentTransactions) {
            Date transDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(transaction.date);
            assertTrue(transDate.after(startDateFormatted) && transDate.before(endDateFormatted));
        }
    }
}


package report;

import messaging.Event;
import messaging.EventSender;

/** @author Helena */
public class ReportHelper {
    public ReportHelper() {
    }

    private Event event;

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}

package report;

import dto.Transaction;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.util.UUID;

public class TransactionSteps {
    ReportService reportService;
    Transaction transaction;

    public TransactionSteps(MockReportService mockReportService) {
        reportService = mockReportService;
    }

    /** @author Helena */
    @When("I receive an event with type {string} and action {string}")
    public void iRecieveAnEventWithTypeAndAction(String eventType, String eventAction) throws Exception {
        transaction = new Transaction("cid", "mid", new BigDecimal(10), "Demo", UUID.randomUUID().toString());
        Event e = new Event(UUID.randomUUID(), eventType, eventAction, new Object[]{transaction});
        reportService.receiveEvent(e);
    }

    /** @author Zenia */
    @Then("The transaction in the event is added to a list of transactions")
    public void theTransactionInTheMessageIsAddedToAListOfTransactions() {
        assertTrue(reportService.getTransactions().stream().anyMatch(t -> t.equals(transaction)));
    }
}


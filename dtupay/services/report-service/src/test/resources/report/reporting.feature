Feature: Reporting
  # @author Frederik
  Scenario: A manager requests a list of reports
    When I receive an event with type "report" and action "saveTransaction"
    When I receive an event with type "report" and action "allTransactions"
    Then I send an event with type "dtuPay" and action "allTransactions"
    And The sent event contains a list of all transactions

    # @author Oscar
  Scenario: A manager requests a list of reports within a date interval
    Given A transaction with cid "cid" and date "2015-05-21"
    And A transaction with cid "cid" and date "2020-05-21"
    And A transaction with cid "cid" and date "2021-05-21"
    And A date interval with startDate "2019-01-01" and endDate "2022-01-01"
    When I receive an event with type "report" and action "customerTransactions" and cid "cid" and date interval
    Then I send an event with type "dtuPay" and action "customerTransactions"
    And The sent event contains a list of transactions in the interval
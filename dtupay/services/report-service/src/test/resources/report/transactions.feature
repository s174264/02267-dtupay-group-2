Feature: Transaction
  # @author Helena
  Scenario: A transaction is sent to be stored in transaction
    When I receive an event with type "report" and action "saveTransaction"
    Then The transaction in the event is added to a list of transactions

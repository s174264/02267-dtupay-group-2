import dto.Merchant;
import messaging.Event;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

/** @author Emil */
@Path("/merchants")
public class MerchantResource {
    DTUPay dtuPay = new DTUPayFactory().getService();

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(Merchant merchant) throws Exception {
        UUID correlationId = UUID.randomUUID();
        Event event = new Event(correlationId, "account", "createMerchant", new Object[]{merchant});
        Event expectedEvent = new Event(correlationId, "dtuPay", "createdMerchant");

        var response = dtuPay.sendAndReceiveEvent(event, expectedEvent);

        if (response.getHasError()) {
            return Response.status(400).entity(response.getErrorMessage()).build();
        }

        return Response.status(201).build();
    }
}

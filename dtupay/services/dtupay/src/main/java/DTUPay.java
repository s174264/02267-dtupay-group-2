import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;
import messaging.FutureEvent;

public class DTUPay implements EventReceiver {
    private final EventSender eventSender;
    private final Map<UUID, FutureEvent> resultsMap = new HashMap<>();

    public DTUPay(EventSender eventSender) {
        this.eventSender = eventSender;
    }

    /**
     * @author Helena
     * Sends an event to the Event Queue, without waiting for a specific response.
     */
    public void sendEvent(Event event) throws Exception {
        eventSender.sendEvent(event);
    }

    /**
     * @author Mai-Thi
     * Sends an event to the Event Queue and waits for the expectedEvent to be returned to the Event Queue.
     */
    public Event sendAndReceiveEvent(Event sendingEvent, Event expectedEvent) throws Exception {
        CompletableFuture<Event> eventFuture = new CompletableFuture<>();

        resultsMap.put(sendingEvent.getCorrelationId(), new FutureEvent(expectedEvent, eventFuture));

        eventSender.sendEvent(sendingEvent);

        return eventFuture.join();
    }

    /**
     * @author Emil
     * Recieves an Event and checks that a client is expecting a related event.
     */
    @Override
    public void receiveEvent(Event event) throws Exception {
        if (!resultsMap.containsKey(event.getCorrelationId())) {
            // Handle "standalone" events.
            handleEvent(event);

            return;
        }

        FutureEvent futureEvent = resultsMap.get(event.getCorrelationId());
        Event expectedEvent = futureEvent.getExpectedEvent();
        if (!event.same(expectedEvent)) {
            return;
        }

        resultsMap.remove(event.getCorrelationId());

        futureEvent.getEventFuture().complete(event);
    }

    public void handleEvent(Event event) throws Exception {
        // Handler of "standalone" events
    }
}

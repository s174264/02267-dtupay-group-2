import dto.Customer;
import messaging.Event;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

/** @author Frederik */
@Path("/customers")
public class CustomerResource {
    DTUPay dtuPay = new DTUPayFactory().getService();

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(Customer customer) throws Exception {
        UUID correlationId = UUID.randomUUID();
        Event event = new Event(correlationId, "account", "createCustomer", new Object[]{customer});
        Event expectedEvent = new Event(correlationId, "dtuPay", "createdCustomer");

        var response = dtuPay.sendAndReceiveEvent(event, expectedEvent);

        if (response.getHasError()) {
            return Response.status(400).entity(response.getErrorMessage()).build();
        }

        return Response.status(201).build();
    }
}

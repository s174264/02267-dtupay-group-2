import dto.ClearToken;
import dto.CreateToken;
import messaging.Event;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;

/** @author Oscar */
@Path("/tokens")
public class TokenResource {
	DTUPay dtuPay = new DTUPayFactory().getService();

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response generateTokens(CreateToken createToken) throws Exception {
		UUID correlationId = UUID.randomUUID();
		Event event = new Event(correlationId, "token", "create", new Object[]{createToken});
		Event expectedEvent = new Event(correlationId, "token", "created");

		Event response = dtuPay.sendAndReceiveEvent(event, expectedEvent);
		if (response.getHasError()) {
			return Response.status(400).entity(response.getErrorMessage()).build();
		}

		List<String> createdTokens = response.getArgumentsAsArray(String.class);
		return Response.status(201).entity(createdTokens).build();
	}

	@POST
	@Path("/clear")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response clearTokens(ClearToken clearToken) throws Exception {
		UUID correlationId = UUID.randomUUID();
		Event event = new Event(correlationId, "token", "clear", new Object[]{clearToken});
		Event expectedEvent = new Event(correlationId, "token", "cleared");

		Event response = dtuPay.sendAndReceiveEvent(event, expectedEvent);

		if (response.getHasError()) {
			return Response.status(400).entity(response.getErrorMessage()).build();
		}

		return Response.status(204).build();
	}
}

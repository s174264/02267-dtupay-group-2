import dto.ReportParameter;
import dto.Transaction;
import messaging.Event;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;

/** @author Zenia */
@Path("/reports")
public class ReportResource {
	DTUPay dtuPay = new DTUPayFactory().getService();

	@GET
	public Response getTransactionsReport() throws Exception {
        UUID correlationId = UUID.randomUUID();
		Event event = new Event(correlationId, "report", "allTransactions");
		Event expectedEvent = new Event(correlationId, "dtuPay", "allTransactions");

		Event response = dtuPay.sendAndReceiveEvent(event, expectedEvent);

		List<Transaction> transactions = response.getArgumentsAsObjectArray(Transaction.class);
		return Response.status(200).entity(transactions).build();
	}

	@Path("{cid}")
	@GET
	public Response getCustomerTransactionsReport(@PathParam("cid") String cid, @QueryParam("startDate") String startDate, @QueryParam("endDate") String endDate) throws Exception {
		UUID correlationId = UUID.randomUUID();
		Event event = new Event(correlationId, "report", "customerTransactions", new Object[]{new ReportParameter(cid, startDate, endDate)});
		Event expectedEvent = new Event(correlationId, "dtuPay", "customerTransactions");

		Event response = dtuPay.sendAndReceiveEvent(event, expectedEvent);

		List<Transaction> transactions = response.getArgumentsAsObjectArray(Transaction.class);
		return Response.status(200).entity(transactions).build();
	}

}

import dto.Payment;
import messaging.Event;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

/** @author Mai-Thi */
@Path("/payments")
public class PaymentResource {
	DTUPay dtuPay = new DTUPayFactory().getService();

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response pay(Payment payment) throws Exception {
		Object[] payload = new Object[]{payment};

        UUID correlationId = UUID.randomUUID();
		Event event = new Event(correlationId, "token", "validate", payload);
		Event expectedEvent = new Event(correlationId, "dtuPay", "createdPayment");

		Event response = dtuPay.sendAndReceiveEvent(event, expectedEvent);

		if (response.getHasError()) {
			return Response.status(400).entity(response.getErrorMessage()).build();
		}

        return Response.status(201).build();
	}
}

import messaging.EventSender;
import messaging.rabbitmq.RabbitMqListener;
import messaging.rabbitmq.RabbitMqSender;

public class DTUPayFactory {
	static DTUPay service = null;

	public DTUPay getService() {
		if (service != null) {
			return service;
		}

		String rabbitHost = "127.0.0.1";
		String rabbitPrefix = "dtupay";

		// If a system variable is set specifying message queue host use it. Otherwise 127.0.0.1 is used (localhost).
        if (System.getenv().containsKey("RABBITMQ_HOST") && !System.getenv("RABBITMQ_HOST").equals("")) {
            rabbitHost = System.getenv("RABBITMQ_HOST");
        }

		EventSender eventSender = new RabbitMqSender(rabbitHost, rabbitPrefix);
		service = new DTUPay(eventSender);
		RabbitMqListener r = new RabbitMqListener(service, rabbitHost, rabbitPrefix);

		try {
			r.listen();
		} catch (Exception e) {
			throw new Error(e);
		}

		return service;
	}
}

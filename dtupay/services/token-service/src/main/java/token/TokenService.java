package token;

import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;
import token.dto.ClearToken;
import token.dto.CreateToken;
import token.dto.Payment;

import java.util.*;

public class TokenService implements EventReceiver {
    EventSender sender;

    /*** This dictionary maps a customerId to a list of tokens that belong to that customer.
     *   A token that belongs to a customer is valid and is removed when it has been used*/
    Map<String, List<String>> customerTokensMap = new HashMap<>();

    public TokenService(EventSender sender) {
        this.sender = sender;
    }

    /** @author Oscar */
    @Override
    public void receiveEvent(Event event) throws Exception {
        if (event.getEventType().equals("token")) {

            /**
             * This block of code validates the token that has been supplied with
             * the information required to make a payment. The token
             */
            if (event.getEventAction().equals("validate")) {
                Payment payment = event.getArgumentsAsObject(Payment.class);

                // Check if the token provided matches any token that is stored.
                // If a match is found, the token provided must be valid and the id of the customer is returned
                String cid = findCustomerByToken(payment.token);
                if (cid == null) {
                    // The token is not valid. Reject the payment
                    Event e = new Event(event.getCorrelationId(), "dtuPay", "createdPayment", true, "Token not valid");
                    sender.sendEvent(e);
                    return;
                }

                // The provided token is valid.
                // Remove token from the customer as it has been used
                var customerTokens = customerTokensMap.get(cid);
                customerTokens.remove(payment.token);

                // Set the customer id that has been found from the token and send the payment to account validation
                payment.cid = cid;

                Event e = new Event(event.getCorrelationId(), "account", "validate", new Object[]{payment});
                sender.sendEvent(e);
                return;
            }

            /**
             * This block of code creates tokens when requested.
             * The request is validated to make sure that
             * the customer can request 1 to 5 tokens only when they have 0 or 1 unused token
             */
            if (event.getEventAction().equals("create")) {
                CreateToken createToken = event.getArgumentsAsObject(CreateToken.class);

                String errorMessage = null;
                if (createToken.amount < 1) {
                    errorMessage = "Customer requested no tokens";
                } else if (createToken.amount > 5) {
                    errorMessage = "Token request limit exceeded";
                }

                List<String> tokens = customerTokensMap.get(createToken.cid);
                if (tokens != null && tokens.size() > 1) {
                    errorMessage = "Customer has more than 1 valid token";
                }

                if (errorMessage != null) {
                    Event e = new Event(event.getCorrelationId(), "token", "created", true, errorMessage);
                    sender.sendEvent(e);
                    return;
                }

                // Create the tokens for the user and add the tokens to the customer in the map
                // Then include the tokens in the message
                if (tokens == null) {
                    tokens = generateTokens(createToken.amount);
                } else {
                    tokens.addAll(generateTokens(createToken.amount));
                }

                customerTokensMap.put(createToken.cid, tokens);

                Event e = new Event(event.getCorrelationId(), "token", "created", tokens.toArray());
                sender.sendEvent(e);
            }

            /**
             * This block of code removes all tokens belonging to a given customer
             */
            if (event.getEventAction().equals("clear")) {
                var clearToken = event.getArgumentsAsObject(ClearToken.class);

                List<String> existingTokens = customerTokensMap.get(clearToken.cid);

                if (existingTokens != null) {
                    customerTokensMap.get(clearToken.cid).clear();
                }

                Event e = new Event(event.getCorrelationId(), "token", "cleared");
                sender.sendEvent(e);

            }
        } else {
            System.out.println("token-service: event ignored: " + event);
        }
    }

    /**
     * @author Frederik
     * This method finds the customer in the dictionary that matches the token provided. If the token is invalid
     * and therefore not in the dictionary, the customer will not be found.
     * If the method returns a cid, the token must therefore be valid
     */
    private String findCustomerByToken(String token) {
        for (var entry : customerTokensMap.entrySet()) {
            Optional<String> t1 = entry.getValue().stream().filter(t -> t.equals(token)).findFirst();

            if (t1.isPresent()) {
                return entry.getKey();
            }
        }

        return null;
    }

    /**
     * @author Frederik
     * This method generates a list of tokens using random UUID's.
     */
    public List<String> generateTokens(int amount) {
        List<String> tokens = new ArrayList<>();

        for (int i = 0; i < amount; i++) {
            tokens.add(UUID.randomUUID().toString());
        }

        return tokens;
    }

    public void setTokenDict(Map<String, List<String>> customerDict) {
        this.customerTokensMap = customerDict;
    }
}

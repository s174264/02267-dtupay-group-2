package token.dto;

public class Payment {
    public String cid;
    public String mid;
    public int amount;
    public String description;
    public String token;

    public Payment() {
    }

    public Payment(String cid, String mid, int amount, String description, String token) {
        this.cid = cid;
        this.mid = mid;
        this.amount = amount;
        this.description = description;
        this.token = token;
    }
}
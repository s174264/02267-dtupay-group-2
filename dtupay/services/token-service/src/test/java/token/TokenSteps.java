package token;

import static org.junit.jupiter.api.Assertions.*;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.EventSender;
import token.dto.ClearToken;
import token.dto.CreateToken;
import token.dto.Payment;

import java.util.*;

public class TokenSteps {
    TokenService tokenService;
    Event event;

    String token;

    public TokenSteps() {
        tokenService = new TokenService(new EventSender() {

            @Override
            public void sendEvent(Event ev) throws Exception {
                event = ev;
            }

        });
    }

    /** @author Frederik */
    @Given("a customer with cid {string} and {int} valid tokens")
    public void aCustomerWithCidAndTokens(String cid, int tokenAmount) {
        Map<String, List<String>> tokenDict = new HashMap<>();
        List<String> tokens = tokenService.generateTokens(tokenAmount);
        tokenDict.put(cid, tokens);

        tokenService.setTokenDict(tokenDict);

        if (tokens.size() > 0) {
            token = tokens.get(0);
        }
    }

    /** @author Emil */
    @Given("an invalid token")
    public void anInvalidToken() {
        token = UUID.randomUUID().toString();
    }

    /** @author Oscar */
    @When("I receive the event with type {string} and action {string} and cid {string} requesting {int} tokens")
    public void iReceiveTheEventWithActionAndCidRequestingTokens(String type, String action, String cid, int amount) throws Exception {
        CreateToken createToken = new CreateToken(cid, amount);
        tokenService.receiveEvent(new Event(UUID.randomUUID(), type, action, new Object[]{createToken}));
    }

    /** @author Emil */
    @Then("I send the event with type {string} and action {string} containing {int} tokens")
    public void iSendTheEventContainingTokens(String type, String action, int amount) {
        assertEquals(type, event.getEventType());
        assertEquals(action, event.getEventAction());
        assertEquals(amount, event.getArgumentsAsArray(String.class).size());
    }

    /** @author Frederik */
    @Then("I send the event with type {string} and action {string} and error message {string}")
    public void iSendTheEventWithTypeAndActionAndErrormessage(String type, String action, String errorMessage) {
        assertEquals(type, event.getEventType());
        assertEquals(action, event.getEventAction());
        assertEquals(errorMessage, event.getErrorMessage());
    }

    /** @author Oscar */
    @When("I receive the event with type {string} and action {string} and a token")
    public void iReceiveTheEventWithTypeAndActionAndAToken(String type, String action) throws Exception {
        Payment payment = new Payment(null, "1", 10, "desc", token);
        tokenService.receiveEvent(new Event(UUID.randomUUID(), type, action, new Object[]{payment}));
    }

    /** @author Emil */
    @When("I receive the event with type {string} and action {string} and cid {string}")
    public void iReceiveTheEventWithTypeAndActionAndCid(String type, String action, String cid) throws Exception {
        ClearToken clearToken = new ClearToken(cid);
        tokenService.receiveEvent(new Event(UUID.randomUUID(), type, action, new Object[]{clearToken}));
    }

    /** @author Frederik */
    @Then("I send the event with type {string} and action {string} and cid {string}")
    public void iSendTheEventWithTypeAndActionAndCid(String type, String action, String cid) {
        assertEquals(type, event.getEventType());
        assertEquals(action, event.getEventAction());
        assertEquals(cid, event.getArgumentsAsObject(Payment.class).cid);
    }

    /** @author Oscar */
    @Then("I send the event with type {string} and action {string}")
    public void iSendTheEventWithTypeAndAction(String type, String action) {
        assertEquals(type, event.getEventType());
        assertEquals(action, event.getEventAction());
    }

    /** @author Frederik */
    @Then("the customer with cid {string} has {int} tokens")
    public void theCustomerWithCidHasTokens(String cid, int amount) {
        assertEquals(amount, tokenService.customerTokensMap.get(cid).size());
    }
}


Feature: Create tokens feature
  # @author Emil
  Scenario: Customer successfully requests more tokens
    Given a customer with cid "cid" and 0 valid tokens
    When I receive the event with type "token" and action "create" and cid "cid" requesting 5 tokens
    Then I send the event with type "token" and action "created" containing 5 tokens

    # @author Frederik
  Scenario: Customer successfully requests more tokens
    Given a customer with cid "cid" and 1 valid tokens
    When I receive the event with type "token" and action "create" and cid "cid" requesting 5 tokens
    Then I send the event with type "token" and action "created" containing 6 tokens

    # @author Oscar
  Scenario: Customer unsuccessfully requests more tokens because they have more than 1 valid token
    Given a customer with cid "cid" and 2 valid tokens
    When I receive the event with type "token" and action "create" and cid "cid" requesting 5 tokens
    Then I send the event with type "token" and action "created" and error message "Customer has more than 1 valid token"

    # @author Frederik
  Scenario: Customer unsuccessfully requests more tokens than legal
    Given a customer with cid "cid" and 0 valid tokens
    When I receive the event with type "token" and action "create" and cid "cid" requesting 10 tokens
    Then I send the event with type "token" and action "created" and error message "Token request limit exceeded"

    # @author Emil
  Scenario: Customer unsuccessfully requests no tokens
    Given a customer with cid "cid" and 0 valid tokens
    When I receive the event with type "token" and action "create" and cid "cid" requesting 0 tokens
    Then I send the event with type "token" and action "created" and error message "Customer requested no tokens"

    # @author Oscar
  Scenario: Customer unsuccessfully requests negative amount of tokens
    Given a customer with cid "cid" and 0 valid tokens
    When I receive the event with type "token" and action "create" and cid "cid" requesting -1 tokens
    Then I send the event with type "token" and action "created" and error message "Customer requested no tokens"
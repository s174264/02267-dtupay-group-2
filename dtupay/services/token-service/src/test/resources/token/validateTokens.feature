Feature: Validate tokens
  # @author Frederik
  Scenario: Token successfully validated
    Given a customer with cid "cid" and 1 valid tokens
    When I receive the event with type "token" and action "validate" and a token
    Then I send the event with type "account" and action "validate" and cid "cid"

    # @author Oscar
  Scenario: Invalid token not validated
    Given a customer with cid "cid" and 0 valid tokens
    And an invalid token
    When I receive the event with type "token" and action "validate" and a token
    Then I send the event with type "dtuPay" and action "createdPayment" and error message "Token not valid"
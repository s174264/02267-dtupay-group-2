Feature: Clear tokens feature
  # @author Frederik
  Scenario: Customer successfully clears tokens
    Given a customer with cid "cid" and 1 valid tokens
    When I receive the event with type "token" and action "clear" and cid "cid"
    Then I send the event with type "token" and action "cleared"
    And the customer with cid "cid" has 0 tokens

    # @author Oscar
  Scenario: Customer with no tokens successfully clears tokens
    Given a customer with cid "cid" and 0 valid tokens
    When I receive the event with type "token" and action "clear" and cid "cid"
    Then I send the event with type "token" and action "cleared"
    And the customer with cid "cid" has 0 tokens
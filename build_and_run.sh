#!/bin/bash
set -e

# BUILD MESSAGE UTILITIES
pushd dtupay/libraries/message-utilities
./build.sh
popd

# BUILD DTU BANK
pushd dtupay/libraries/dtubank
./build.sh
popd

# BUILD ACCOUNT SERVICE
pushd dtupay/services/account-service
./build.sh
popd

# BUILD PAYMENT SERVICE
pushd dtupay/services/payment-service
./build.sh
popd

# BUILD REPORT SERVICE
pushd dtupay/services/report-service
./build.sh
popd

# BUILD TOKEN SERVICE
pushd dtupay/services/token-service
./build.sh
popd

# BUILD DTUPAY REST
pushd dtupay/services/dtupay
./build.sh
popd

pushd clients
./deploy.sh
sleep 10s
./test.sh
popd

# Cleanup the build images
docker image prune -f

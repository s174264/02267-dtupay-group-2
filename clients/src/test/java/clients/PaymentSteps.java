package clients;

import clients.dto.*;
import dtubank.BankService;
import dtubank.BankServiceService;
import dtubank.*;
import io.cucumber.java.After;
import io.cucumber.java.en.*;
import org.junit.Before;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class PaymentSteps {
    BankService bank = new BankServiceService().getBankServicePort();
    MerchantAPI merchantAPI = new MerchantAPI();
    CustomerAPI customerAPI = new CustomerAPI();

    CustomerHelper customerHelper;
    MerchantHelper merchantHelper;
    PaymentHelper paymentHelper;

    List<String> accounts = new ArrayList<>();

    boolean successful;

    public PaymentSteps(CustomerHelper customerHelper, MerchantHelper merchantHelper, PaymentHelper paymentHelper) {
        this.customerHelper = customerHelper;
        this.merchantHelper = merchantHelper;
        this.paymentHelper = paymentHelper;
    }

    @Before()
    public void openHttpClient() {
        merchantAPI.startClient();
        customerAPI.startClient();
    }

    @After()
    public void closeHttpClient() {
        merchantAPI.closeClient();
        customerAPI.closeClient();
    }

    @After()
    public void deleteUsers() {
        for (String account : accounts) {
            try {
                bank.retireAccount(account);
            } catch (BankServiceException_Exception e) {
            }
        }
    }

    /** @author Zenia */
    @Given("the customer {string} {string} with CPR {string} has a bank account with balance {int}")
    public void theCustomerWithCPRHasABankAccountWithBalance(String firstName, String lastName, String cpr, int balance) throws BankServiceException_Exception {
        User user = new User();
        user.setCprNumber(cpr);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        customerHelper.setCustomer(user);

        try {
            bank.retireAccount(bank.getAccountByCprNumber(cpr).getId());
        } catch (BankServiceException_Exception e) {
        }

        customerHelper.setCid(bank.createAccountWithBalance(customerHelper.getCustomer(), new BigDecimal(balance)));
        accounts.add(customerHelper.getCid());
    }

    /** @author Oscar */
    @Given("the customer is registered with DTUPay")
    public void theCustomerIsRegisteredWithDTUPay() {
        assertTrue(customerAPI.registerCustomer(new Customer(customerHelper.getCid())));
    }

    /** @author Mai-Thi */
    @Given("the merchant {string} {string} with CPR number {string} has a bank account with balance {int}")
    public void theMerchantWithCPRNumberHasABankAccountWithBalance(String firstName, String lastName, String cpr, int balance) throws BankServiceException_Exception {
        User user = new User();
        user.setCprNumber(cpr);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        merchantHelper.setMerchant(user);

        try {
            bank.retireAccount(bank.getAccountByCprNumber(cpr).getId());
        } catch (BankServiceException_Exception e) {
        }

        merchantHelper.setMid(bank.createAccountWithBalance(merchantHelper.getMerchant(), new BigDecimal(balance)));
        accounts.add(merchantHelper.getMid());
    }

    /** @author Helena */
    @Given("the merchant is registered with DTUPay")
    public void theMerchantIsRegisteredWithDTUPay() {
        assertTrue(merchantAPI.registerMerchant(new Merchant(merchantHelper.getMid())));
    }

    /** @author Frederik */
    @Given("the customer has a valid token")
    public void theCustomerHasAValidToken() throws Exception {
        customerAPI.clearTokens(new ClearToken(customerHelper.getCid()));
        List<String> tokens = customerAPI.requestTokens(new CreateToken(customerHelper.getCid(), 5));
        customerHelper.setTokens(tokens);
    }

    /** @author Emil */
    @When("the merchant initiates a payment for {int} kr with the description {string} by the customer")
    public void theMerchantInitiatesAPaymentForKrWithTheDescriptionByTheCustomer(int amount, String description) {
        paymentHelper.setPayment(new Payment(merchantHelper.getMid(), new BigDecimal(amount), description, customerHelper.getValidToken()));
        successful = merchantAPI.pay(paymentHelper.getPayment());
    }

    /** @author Oscar */
    @When("the customer's account is retired")
    public void theCustomerSAccountIsRetired() throws BankServiceException_Exception {
        bank.retireAccount(bank.getAccountByCprNumber(customerHelper.getCustomer().getCprNumber()).getId());
    }

    /** @author Helena */
    @When("the merchant's account is retired")
    public void theMerchantSAccountIsRetired() throws BankServiceException_Exception {
        bank.retireAccount(bank.getAccountByCprNumber(merchantHelper.getMerchant().getCprNumber()).getId());
    }

    /** @author Mai-Thi */
    @When("the merchant initiates a payment for {int} kr with the description {string} by the customer with invalid token")
    public void theMerchantInitiatesAPaymentForKrWithTheDescriptionByTheCustomerWithInvalidToken(int amount, String description) {
        paymentHelper.setPayment(new Payment(merchantHelper.getMid(), new BigDecimal(amount), description, /*Invalid token*/"invalidToken"));
        successful = merchantAPI.pay(paymentHelper.getPayment());
    }

    /** @author Emil */
    @Then("the payment is successful")
    public void thePaymentIsSuccessful() {
        assertTrue(successful);
    }

    /** @author Zenia */
    @Then("the balance of the customer at the bank is {int} kr")
    public void theBalanceOfTheCustomerAtTheBankIsKr(int newBalance) throws BankServiceException_Exception {
        assertEquals(bank.getAccount(customerHelper.getCid()).getBalance().doubleValue(), new BigDecimal(newBalance).doubleValue());
    }

    /** @author Mai-Thi */
    @Then("the balance of the merchant at the bank is {int} kr")
    public void theBalanceOfTheMerchantAtTheBankIsKr(int newBalance) throws BankServiceException_Exception {
        assertEquals(bank.getAccount(merchantHelper.getMid()).getBalance().doubleValue(), new BigDecimal(newBalance).doubleValue());
    }

    /** @author Frederik */
    @Then("the payment is not successful")
    public void thePaymentIsNotSuccessful() {
        assertFalse(successful);
    }

    /** @author Helena */
    @Then("an error message is returned saying {string}")
    public void anErrorMessageIsReturnedSaying(String errorMsg) {
        assertEquals(errorMsg, merchantAPI.errorMessage);
    }


}

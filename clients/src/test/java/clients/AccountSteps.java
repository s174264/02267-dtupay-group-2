package clients;

import clients.dto.*;
import dtubank.BankService;
import dtubank.BankServiceException_Exception;
import dtubank.BankServiceService;
import io.cucumber.java.After;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Before;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class AccountSteps {
    BankService bank = new BankServiceService().getBankServicePort();
    MerchantAPI merchantAPI = new MerchantAPI();
    CustomerAPI customerAPI = new CustomerAPI();

    CustomerHelper customerHelper;
    MerchantHelper merchantHelper;
    PaymentHelper paymentHelper;

    List<String> accounts = new ArrayList<>();
    boolean successful;

    public AccountSteps(CustomerHelper customerHelper, MerchantHelper merchantHelper, PaymentHelper paymentHelper) {
        this.customerHelper = customerHelper;
        this.merchantHelper = merchantHelper;
        this.paymentHelper = paymentHelper;
    }

    @Before()
    public void openHttpClient() {
        merchantAPI.startClient();
        customerAPI.startClient();
    }

    @After()
    public void closeHttpClient() {
        merchantAPI.closeClient();
        customerAPI.closeClient();
    }

    @After()
    public void deleteUsers() {
        for (String account : accounts) {
            try {
                bank.retireAccount(account);
            } catch (BankServiceException_Exception e) {
            }
        }
    }

    /** @author Emil */
    @When("the customer registers with DTUPay")
    public void theCustomerRegistersWithDTUPay() {
        successful = customerAPI.registerCustomer(new Customer(customerHelper.getCid()));
    }

    /** @author Frederik */
    @When("the merchant registers with DTUPay")
    public void theMerchantRegistersWithDTUPay() {
        successful = merchantAPI.registerMerchant(new Merchant(merchantHelper.getMid()));
    }

    /** @author Oscar */
    @Then("the registration is successful")
    public void theRegistrationIsSuccessful() {
        assertTrue(successful);
    }

    /** @author Frederik */
    @Then("an error message for the customer is returned saying {string}")
    public void anErrorMessageForTheCustomerIsReturnedSaying(String errorMsg) {
        assertEquals(errorMsg, customerAPI.errorMessage);
    }

    /** @author Frederik */
    @Then("an error message for the merchant is returned saying {string}")
    public void anErrorMessageForTheMerchantIsReturnedSaying(String errorMsg) {
        assertEquals(errorMsg, merchantAPI.errorMessage);
    }
}

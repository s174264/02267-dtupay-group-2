package clients;

import clients.dto.Customer;
import clients.dto.Payment;
import clients.dto.Transaction;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Before;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class ReportSteps {

    CustomerAPI customerAPI;
    ManagerAPI managerAPI;
    CustomerHelper customerHelper;
    List<Transaction> allTransactions = new ArrayList<>();
    List<Transaction> customerTransactions = new ArrayList<>();
    PaymentHelper paymentHelper;
    String startDate;
    String endDate;

    public ReportSteps(PaymentHelper paymentHelper, CustomerHelper customerHelper, CustomerAPI customerAPI, ManagerAPI managerAPI) {
        this.paymentHelper = paymentHelper;
        this.customerHelper = customerHelper;
        this.customerAPI = customerAPI;
        this.managerAPI = managerAPI;
    }

    @Before()
    public void openHttpClient() {
        managerAPI.startClient();
        customerAPI.startClient();
    }

    @After()
    public void closeHttpClient() {
        managerAPI.closeClient();
        customerAPI.closeClient();
    }

    /** @author Helena */
    @Given("there is a DTUPay manager")
    public void thereIsADTUPayManager() {
        assertNotNull(managerAPI);
    }

    /** @author Mai-Thi */
    @Given("there is a start date today and an end date today")
    public void thereIsAStartDateTodayAndAnEndDateToday() {
        startDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        endDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    /** @author Zenia */
    @Given("there is a start date tomorrow and an end date tomorrow")
    public void thereIsAStartDateTomorrowAndAnEndDateTomorrow() {
        startDate = new SimpleDateFormat("yyyy-MM-dd").format(Date.from(new Date().toInstant().plus(Duration.ofDays(1))));
        endDate = new SimpleDateFormat("yyyy-MM-dd").format(Date.from(new Date().toInstant().plus(Duration.ofDays(1))));
    }

    /** @author Helena */
    @Given("there is a start date yesterday and an end date yesterday")
    public void thereIsAStartDateYesterdayAndAnEndDateYesterday() {
        startDate = new SimpleDateFormat("yyyy-MM-dd").format(Date.from(new Date().toInstant().minus(Duration.ofDays(1))));
        endDate = new SimpleDateFormat("yyyy-MM-dd").format(Date.from(new Date().toInstant().minus(Duration.ofDays(1))));
    }

    /** @author Mai-Thi */
    @When("the manager requests all transactions")
    public void theManagerRequestsAllTransactions() {
        allTransactions = managerAPI.getAllTransactions();
    }

    /** @author Zenia */
    @Then("the manager can see a list of all transactions")
    public void theManagerCanSeeAListOfAllTransactions() {
        Payment payment = paymentHelper.getPayment();
        Transaction transaction = allTransactions.get(allTransactions.size()-1);
        assertFalse(transaction.cid.isEmpty());
        assertFalse(transaction.date.isEmpty());
        assertEquals(payment.mid,transaction.mid);
        assertEquals(payment.amount.doubleValue(), transaction.amount.doubleValue());
        assertEquals(payment.description,transaction.description);
        assertEquals(payment.token,transaction.token);
    }

    /** @author Helena */
    @When("the customer requests all transactions in the date interval")
    public void theCustomerRequestsAllTransactionsInTheDateInterval() {
        customerTransactions = customerAPI.getTransactionsFromInterval(new Customer(customerHelper.getCid()), startDate, endDate);
    }

    /** @author Mai-Thi */
    @When("another customer requests all transactions in the date interval")
    public void anotherCustomerRequestsAllTransactionsInTheDateInterval() {
        customerTransactions = customerAPI.getTransactionsFromInterval(new Customer("1"), startDate, endDate);
    }

    /** @author Zenia */
    @Then("the customer can see a list of all their transactions in the date interval")
    public void theCustomerCanSeeAListOfAllTheirTransactionsInTheDateInterval() {
        Payment payment = paymentHelper.getPayment();
        Transaction transaction = customerTransactions.get(customerTransactions.size()-1);
        assertFalse(transaction.cid.isEmpty());
        assertFalse(transaction.date.isEmpty());
        assertEquals(payment.mid,transaction.mid);
        assertEquals(payment.amount.doubleValue(), transaction.amount.doubleValue());
        assertEquals(payment.description,transaction.description);
        assertEquals(payment.token,transaction.token);
    }

    /** @author Zenia */
    @Then("the customer gets an empty list of transactions")
    public void theCustomerGetsAnEmptyListOfTransactions() {
        assertTrue(customerTransactions.isEmpty());
    }
}

package clients;

import clients.dto.Transaction;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import java.util.ArrayList;
import java.util.List;

public class ManagerAPI extends GenericAPI {

    public ManagerAPI() {
        super();
    }
    /**
     * @author Zenia
     * Get a list of all transactions in DTUPay
     */
    public List<Transaction> getAllTransactions() {
        var response = baseUrl.path("reports").request().get();

        ArrayList<Transaction> transactions = response.readEntity(new GenericType<>() {
        });

        response.close();

        return transactions;
    }
}

package clients;

import clients.dto.Payment;

import java.math.BigDecimal;

/**
 * @author Helena & Oscar
 */
public class PaymentHelper {
    Payment payment;
    CustomerHelper customerHelper;

    public PaymentHelper() {
    }

    public PaymentHelper(CustomerHelper customerHelper) {
        this.customerHelper = customerHelper;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public Payment getSomePayment () {
        if (payment == null) {
            payment = new Payment("mid1", new BigDecimal(200), "Helper", customerHelper.getValidToken());
        }
        return payment;
    }
}

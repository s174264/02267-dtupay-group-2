package clients;

import clients.dto.Merchant;
import clients.dto.Payment;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import java.util.ArrayList;
import java.util.List;

public class MerchantAPI extends GenericAPI {
    String errorMessage = "";

    public MerchantAPI() {
        super();
    }
    /**
     * @author Emil
     * Register a merchant
     */
    public boolean registerMerchant(Merchant merchant) {
        var jsonTrans = Entity.json(merchant);
        var response = baseUrl.path("merchants").request().post(jsonTrans);

        var status = response.getStatus();

        if (status != 201) {
            errorMessage = response.readEntity(String.class);
        }

        response.close();

        return status == 201;
    }
    /**
     * @author Helena
     * Initialize a payment
     */
    public boolean pay(Payment payment) {
        var jsonTrans = Entity.json(payment);
        var response = baseUrl.path("payments").request().post(jsonTrans);

        var status = response.getStatus();

        if (status != 201) {
            errorMessage = response.readEntity(String.class);
        }

        response.close();
        return status == 201;
    }
}

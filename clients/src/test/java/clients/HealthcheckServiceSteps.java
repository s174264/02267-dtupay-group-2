package clients;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Borrowed from the course example projects.
 */
public class HealthcheckServiceSteps {
    String result;
    HealthcheckService service = new HealthcheckService();

    @When("I call the healthcheck service")
    public void iCallTheHelloWorldMerchantService() {
        result = service.healthcheck();
    }

    @Then("I get the answer {string}")
    public void iGetTheAnswer(String string) {
        assertEquals(string,result);
    }
}

package clients.dto;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Transaction {
    public String cid;
    public String mid;
    public BigDecimal amount;
    public String description;
    public String date;
    public String token;

    public Transaction() {
    }

    public Transaction(String cid, String mid, BigDecimal amount, String description, String token) {
        this.cid = cid;
        this.mid = mid;
        this.amount = amount;
        this.description = description;
        this.token = token;
        this.date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());
    }

    public String toString(){
        return "cid: " + cid + " mid: " + mid + " amount: " + amount + " description: " + description + " token: " + token + " date: " + date;
    }
}

package clients.dto;

public class CreateToken {
    public String cid;
    public int amount;

    public CreateToken() {
    }

    public CreateToken(String cid, int amount) {
        this.cid = cid;
        this.amount = amount;
    }
}

package clients.dto;

import java.math.BigDecimal;

public class Payment {
    public String mid;
    public BigDecimal amount;
    public String description;
    public String token;

    public Payment() {
    }

    public Payment(String mid, BigDecimal amount, String description, String token) {
        this.mid = mid;
        this.amount = amount;
        this.description = description;
        this.token = token;
    }
}

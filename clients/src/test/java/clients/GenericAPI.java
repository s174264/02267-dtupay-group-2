package clients;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

public class GenericAPI {
    WebTarget baseUrl;
    Client client;

    public GenericAPI() {
        startClient();
    }

    public void startClient() {
        client = ClientBuilder.newClient();
        baseUrl = client.target("http://localhost:8080/");
    }
    public void closeClient() {
        client.close();
    }
}

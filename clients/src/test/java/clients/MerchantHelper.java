package clients;

import dtubank.User;
/**
 * @author Mai-Thi & Zenia
 */
public class MerchantHelper {
    User user;
    String mid;

    public MerchantHelper() {
    }

    public User getMerchant() {
        return user;
    }

    public void setMerchant(User user) {
        this.user = user;
    }

    public User getSomeMerchant(){
        User user = new User();
        user.setCprNumber("040404-0404");
        user.setFirstName("Birgitte");
        user.setLastName("Birgittedottir");
        return user;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }
}



package clients;


import clients.dto.*;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import java.util.List;

public class CustomerAPI extends GenericAPI {

    String errorMessage;

    public CustomerAPI() {
        super();
    }

    /**
     * @author Frederik
     * Registers a customer and returns true if the registration went well
     */
    public boolean registerCustomer(Customer customer) {
        var jsonTrans = Entity.json(customer);
        var response = baseUrl.path("customers").request().post(jsonTrans);

        var status = response.getStatus();

        if (status != 201) {
            errorMessage = response.readEntity(String.class);
        }

        response.close();

        return status == 201;
    }

    /**
     * @author Oscar
     * Request tokens and returns a list of tokens
     */
    public List<String> requestTokens(CreateToken createToken) throws Exception {
        var json = Entity.json(createToken);
        var response = baseUrl.path("tokens").request().post(json);

        if (response.getStatus() != 201) {
            var errorMessage = response.readEntity(String.class);

            response.close();

            throw new Exception(errorMessage);
        }

        List<String> result = response.readEntity(new GenericType<>() {});

        response.close();

        return result;
    }

    /**
     * @author Oscar
     * Clear all tokens of the customer
     * For testing purpose only
     */
    public void clearTokens(ClearToken clearToken) throws Exception {
        var json = Entity.json(clearToken);
        var response = baseUrl.path("tokens/clear").request().post(json);

        if (response.getStatus() != 204) {
            response.close();

            throw new Exception("Could not clear tokens");
        }

        response.close();
    }

    /**
     * @author Mai-Thi
     * Get a list of a customers transactions between a start and end date
     */
    public List<Transaction> getTransactionsFromInterval(Customer customer, String startDate, String endDate) {
        var response = baseUrl.path("reports/" + customer.id)
                .queryParam("startDate", startDate)
                .queryParam("endDate", endDate)
                .request().get();

        List<Transaction> transactions = response.readEntity(new GenericType<>() {
        });
        response.close();

        return transactions;
    }
}

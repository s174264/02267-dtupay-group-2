package clients;

import clients.dto.ClearToken;
import clients.dto.CreateToken;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Before;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class TokenSteps {
    // Should be checked by account service eventually
    private String cid;

    private String errorMessage;

    private final Map<String, List<String>> tokenDictionary = new HashMap<>();

    CustomerAPI customerAPI = new CustomerAPI();

    @Before()
    public void openHttpClient() {
        customerAPI.startClient();
    }

    @After()
    public void closeHttpClient() {
        customerAPI.closeClient();
    }

    /** @author Oscar */
    @Given("a customer with cid {string}")
    public void aCustomerWithCidIsRegisteredAtDTUPay(String cid) {
        this.cid = cid;
    }

    /** @author Emil */
    @Given("the customer has {int} token\\(s)")
    public void theCustomerHasTokenS(int tokenAmount) throws Exception {
        customerAPI.clearTokens(new ClearToken(cid));
        var tokens = customerAPI.requestTokens(new CreateToken(cid, tokenAmount));

        tokenDictionary.put(cid, tokens);
    }

    /** @author Frederik */
    @Given("the customer has no tokens")
    public void theCustomerHasZeroTokenS() throws Exception {
        customerAPI.clearTokens(new ClearToken(cid));

        tokenDictionary.remove(cid);
    }

    /** @author Oscar */
    @When("the customer asks for {int} tokens")
    public void theCustomerAsksForTokens(int amount) {
        List<String> tokens;
        try {
            tokens = customerAPI.requestTokens(new CreateToken(cid, amount));
            tokenDictionary.put(cid, tokens);
        } catch (Exception e) {
            errorMessage = e.getMessage();
        }
    }

    /** @author Emil */
    @Then("the customer have {int} token\\(s)")
    public void theCustomerHaveTokenS(int tokenAmount) {
        assertEquals(tokenAmount, tokenDictionary.get(cid).size());
    }

    /** @author Frederik */
    @Then("the customer is denied with the message {string}")
    public void theCustomerIsDeniedWithTheMessage(String expectedMessage) {
        assertEquals(expectedMessage, errorMessage);
    }
}

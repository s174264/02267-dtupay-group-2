package clients;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

/**
 * Borrowed from the course example projects.
 */
public class HealthcheckService {
	WebTarget baseUrl;
	String dtuPayEndpoint = "http://localhost:8080/";

	public HealthcheckService() {
		Client client = ClientBuilder.newClient();
		baseUrl = client.target(dtuPayEndpoint);
	}

	public String healthcheck() {
		return baseUrl.path("healthcheck").request().get(String.class);
	}
}

package clients;

import dtubank.User;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Emil & Frederik
 */
public class CustomerHelper {
    String cid;
    User user;
    List<String> tokens = new ArrayList<>();

    public CustomerHelper() {
    }

    public User getCustomer() {
        return user;
    }

    public void setCustomer(User user) {
        this.user = user;
    }

    public List<String> getTokens() {
        return tokens;
    }

    public void setTokens(List<String> tokens) {
        this.tokens = tokens;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getValidToken() {
        return tokens.get(0);
    }

    public User getSomeCustomer() {
        User user = new User();
        user.setCprNumber("030303-0303");
        user.setFirstName("Birger");
        user.setLastName("Birgersen");
        return user;
    }
}
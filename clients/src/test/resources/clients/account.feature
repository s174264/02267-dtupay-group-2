Feature: Account
  # @author Frederik
  Scenario: Successful Register Customer
    Given the customer "Ryan" "Anderson" with CPR "061100-7124" has a bank account with balance 1000
    When the customer registers with DTUPay
    Then the registration is successful

  # @author Oscar
  Scenario: Successful register merchant
    Given the merchant "Anna" "Annasdottir" with CPR number "020202-0202" has a bank account with balance 2000
    When the merchant registers with DTUPay
    Then the registration is successful

  # @author Emil
  Scenario: Customer unsuccessfully tries to register twice
    Given the customer "Ryan" "Anderson" with CPR "061100-7124" has a bank account with balance 1000
    When the customer registers with DTUPay
    Then the registration is successful
    When the customer registers with DTUPay
    Then an error message for the customer is returned saying "The customer is already registered with DTU Pay"

  # @author Frederik
  Scenario: Merchant unsuccessfully tries to register twice
    Given the merchant "Anna" "Annasdottir" with CPR number "020202-0202" has a bank account with balance 2000
    When the merchant registers with DTUPay
    Then the registration is successful
    When the merchant registers with DTUPay
    Then an error message for the merchant is returned saying "The merchant is already registered with DTU Pay"
    
Feature: Payment

  # @author Frederik
  Scenario: Successful Payment
    Given the customer "Allan" "Allansen" with CPR "010101-0101" has a bank account with balance 1000
    And the customer is registered with DTUPay
    And the customer has a valid token
    And the merchant "Anna" "Annadottir" with CPR number "020202-0202" has a bank account with balance 2000
    And the merchant is registered with DTUPay
    When the merchant initiates a payment for 10 kr with the description "payment of ice cream" by the customer
    Then the payment is successful
    And the balance of the customer at the bank is 990 kr
    And the balance of the merchant at the bank is 2010 kr

    # @author Oscar
  Scenario: Failure Transfer more money than balance
    Given the customer "Allan" "Allansen" with CPR "010101-0101" has a bank account with balance 100
    And the customer is registered with DTUPay
    And the customer has a valid token
    And the merchant "Anna" "Annadottir" with CPR number "020202-0202" has a bank account with balance 2000
    And the merchant is registered with DTUPay
    When the merchant initiates a payment for 110 kr with the description "ice cream" by the customer
    Then the payment is not successful
    And an error message is returned saying "Debtor balance will be negative"

    # @author Emil
  Scenario: Failure Description missing
    Given the customer "Allan" "Allansen" with CPR "010101-0101" has a bank account with balance 1000
    And the customer is registered with DTUPay
    And the customer has a valid token
    And the merchant "Anna" "Annadottir" with CPR number "020202-0202" has a bank account with balance 2000
    And the merchant is registered with DTUPay
    When the merchant initiates a payment for 10 kr with the description "" by the customer
    Then the payment is not successful
    And an error message is returned saying "Description missing"

    # @author Zenia
  Scenario: Failure Customer is not registered with DTU Pay
    Given the customer "Allan" "Allansen" with CPR "010101-0101" has a bank account with balance 1000
    And the customer has a valid token
    And the merchant "Anna" "Annasdottir" with CPR number "020202-0202" has a bank account with balance 2000
    And the merchant is registered with DTUPay
    When the merchant initiates a payment for 10 kr with the description "payment of ice cream" by the customer
    Then the payment is not successful
    And an error message is returned saying "Customer is not registered with DTUPay"

    # @author Helena
  Scenario: Failure Merchant is not registered with DTU Pay
    Given the customer "Allan" "Allansen" with CPR "010101-0101" has a bank account with balance 1000
    And the customer is registered with DTUPay
    And the customer has a valid token
    And the merchant "Anna" "Annasdottir" with CPR number "020202-0202" has a bank account with balance 2000
    When the merchant initiates a payment for 10 kr with the description "payment of ice cream" by the customer
    Then the payment is not successful
    And an error message is returned saying "Merchant is not registered with DTUPay"

    # @author Mai-Thi
  Scenario: Failure Customer doesn't have an account
    Given the customer "Allan" "Allansen" with CPR "010101-0101" has a bank account with balance 1000
    And the customer is registered with DTUPay
    And the customer has a valid token
    And the merchant "Anna" "Annadottir" with CPR number "020202-0202" has a bank account with balance 2000
    And the merchant is registered with DTUPay
    When the customer's account is retired
    And the merchant initiates a payment for 10 kr with the description "payment for ice cream" by the customer
    Then the payment is not successful
    And an error message is returned saying "Debtor account does not exist"

    # @author Helena
  Scenario: Failure Merchant doesn't have an account
    Given the customer "Allan" "Allansen" with CPR "010101-0101" has a bank account with balance 1000
    And the customer is registered with DTUPay
    And the customer has a valid token
    And the merchant "Anna" "Annadottir" with CPR number "020202-0202" has a bank account with balance 2000
    And the merchant is registered with DTUPay
    When the merchant's account is retired
    And the merchant initiates a payment for 10 kr with the description "payment for ice cream" by the customer
    Then the payment is not successful
    And an error message is returned saying "Creditor account does not exist"

    # @author Zenia
  Scenario: Failure Negative amount
    Given the customer "Allan" "Allansen" with CPR "010101-0101" has a bank account with balance 1000
    And the customer is registered with DTUPay
    And the customer has a valid token
    And the merchant "Anna" "Annadottir" with CPR number "020202-0202" has a bank account with balance 2000
    And the merchant is registered with DTUPay
    When the merchant initiates a payment for -10 kr with the description "payment for ice cream" by the customer
    Then the payment is not successful
    And an error message is returned saying "Amount must be positive"

    # @author Mai-Thi
  Scenario: Failure invalid token
    Given the customer "Allan" "Allansen" with CPR "010101-0101" has a bank account with balance 1000
    And the customer is registered with DTUPay
    And the merchant "Anna" "Annadottir" with CPR number "020202-0202" has a bank account with balance 2000
    And the merchant is registered with DTUPay
    When the merchant initiates a payment for 10 kr with the description "payment for ice cream" by the customer with invalid token
    Then the payment is not successful
    And an error message is returned saying "Token not valid"

Feature: Report
  # @author Helena
  Scenario: A manager asks for all transactions
    Given there is a DTUPay manager
    # The steps below: given there is a successful payment
    And the customer "Allan" "Allansen" with CPR "010101-0101" has a bank account with balance 1000
    And the customer is registered with DTUPay
    And the customer has a valid token
    And the merchant "Anna" "Annadottir" with CPR number "020202-0202" has a bank account with balance 2000
    And the merchant is registered with DTUPay
    When the merchant initiates a payment for 10 kr with the description "payment of ice cream" by the customer
    Then the payment is successful
    #
    When the manager requests all transactions
    Then the manager can see a list of all transactions

    # @author Zenia
  Scenario: A customer asks for all his transactions for today
    # The steps below: given there is a successful payment
    Given the customer "Allan" "Allansen" with CPR "010101-0101" has a bank account with balance 1000
    And the customer is registered with DTUPay
    And the customer has a valid token
    And the merchant "Anna" "Annadottir" with CPR number "020202-0202" has a bank account with balance 2000
    And the merchant is registered with DTUPay
    When the merchant initiates a payment for 10 kr with the description "payment of ice cream" by the customer
    Then the payment is successful
    #
    Given there is a start date today and an end date today
    When the customer requests all transactions in the date interval
    Then the customer can see a list of all their transactions in the date interval

  # @author Mai-Thi
  Scenario: A customer asks for all his transactions in a given time period where they do not have any transactions (tomorrow)
    # The steps below: given there is a successful payment
    Given the customer "Allan" "Allansen" with CPR "010101-0101" has a bank account with balance 1000
    And the customer is registered with DTUPay
    And the customer has a valid token
    And the merchant "Anna" "Annadottir" with CPR number "020202-0202" has a bank account with balance 2000
    And the merchant is registered with DTUPay
    When the merchant initiates a payment for 10 kr with the description "payment of ice cream" by the customer
    Then the payment is successful
    #
    Given there is a start date tomorrow and an end date tomorrow
    When the customer requests all transactions in the date interval
    Then the customer gets an empty list of transactions

    # @author Helena
  Scenario: A customer asks for all his transactions in a given time period where they do not have any transactions (yesterday)
    # The steps below: given there is a successful payment
    Given the customer "Allan" "Allansen" with CPR "010101-0101" has a bank account with balance 1000
    And the customer is registered with DTUPay
    And the customer has a valid token
    And the merchant "Anna" "Annadottir" with CPR number "020202-0202" has a bank account with balance 2000
    And the merchant is registered with DTUPay
    When the merchant initiates a payment for 10 kr with the description "payment of ice cream" by the customer
    Then the payment is successful
    #
    Given there is a start date yesterday and an end date yesterday
    When the customer requests all transactions in the date interval
    Then the customer gets an empty list of transactions

    # @author Zenia
  Scenario: A customer asks for all his transactions in a given time period where they do not have any transactions but there are other transactions
    # The steps below: given there is a successful payment
    Given the customer "Allan" "Allansen" with CPR "010101-0101" has a bank account with balance 1000
    And the customer is registered with DTUPay
    And the customer has a valid token
    And the merchant "Anna" "Annadottir" with CPR number "020202-0202" has a bank account with balance 2000
    And the merchant is registered with DTUPay
    When the merchant initiates a payment for 10 kr with the description "payment of ice cream" by the customer
    Then the payment is successful
    #
    Given there is a start date today and an end date today
    When another customer requests all transactions in the date interval
    Then the customer gets an empty list of transactions
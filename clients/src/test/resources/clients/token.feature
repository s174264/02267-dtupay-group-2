Feature: Token
  # @author Frederik
  Scenario: Customer with 0 tokens asks for 3 new tokens
    Given a customer with cid "UNIQUE_COSTOMER_ID"
    And the customer has no tokens
    When the customer asks for 3 tokens
    Then the customer have 3 token(s)

    # @author Oscar
  Scenario: Customer with 1 tokens asks for new tokens
    Given a customer with cid "UNIQUE_COSTOMER_ID1"
    And the customer has 1 token(s)
    When the customer asks for 5 tokens
    Then the customer have 6 token(s)

    # @author Emil
  Scenario: Customer with 0 tokens asks for 10 new tokens
    Given a customer with cid "UNIQUE_COSTOMER_ID"
    And the customer has no tokens
    When the customer asks for 10 tokens
    Then the customer is denied with the message "Token request limit exceeded"

    # @author Frederik
  Scenario: Customer with 2 tokens asks for new tokens
    Given a customer with cid "UNIQUE_COSTOMER_ID2"
    And the customer has 2 token(s)
    When the customer asks for 3 tokens
    Then the customer is denied with the message "Customer has more than 1 valid token"

    # @author Oscar
  Scenario: Customer with 0 tokens asks for negative tokens
    Given a customer with cid "UNIQUE_COSTOMER_ID2"
    And the customer has no tokens
    When the customer asks for -1 tokens
    Then the customer is denied with the message "Customer requested no tokens"

    # @author Emil
  Scenario: Customer with 0 tokens asks for negative tokens
    Given a customer with cid "UNIQUE_COSTOMER_ID2"
    And the customer has no tokens
    When the customer asks for 0 tokens
    Then the customer is denied with the message "Customer requested no tokens"
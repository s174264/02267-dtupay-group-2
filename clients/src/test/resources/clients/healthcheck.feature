Feature: healthcheck service
  Scenario: healthcheck service returns OK
    When I call the healthcheck service
    Then I get the answer "OK"

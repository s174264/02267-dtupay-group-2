#!/bin/bash
set -e
docker image prune -f

docker-compose up -d rabbitmq

sleep 10s

docker-compose up -d account-service payment-service report-service token-service dtupay
